const Command = require("./Command");

class CommandHandler {
    constructor(commandOptions) {
        this.commandOptions = {
            description: "A Discord-based Discord bot",
            ignoreBots: true,
            ignoreSelf: true,
            name: null,
            owner: "an unknown user",
            prefix: "@mention ",
            defaultCommandOptions: {}
        };
        if (typeof commandOptions === "object") {
            for (var property of Object.keys(commandOptions)) {
                this.commandOptions[property] = commandOptions[property];
            }
        }
        this.commands = {};
        this.commandAliases = {};
    };

  registerCommand(label, generator, options) {
      if (label.includes(" ")) {
          throw new Error("Command label may not have spaces");
      }
      if (this.commands[label]) {
          throw new Error("You have already registered a command for " + label);
      }
      options = options || {};
      for (var key in this.commandOptions.defaultCommandOptions) {
          if (options[key] === undefined) {
              options[key] = this.commandOptions.defaultCommandOptions[key];
          }
      }
      this.commands[label] = new Command(label, generator, options);
      if (options.aliases) {
          options.aliases.forEach((alias) => {
              this.commandAliases[alias] = label;
          });
      }
      return this.commands[label];
  }

  unregisterCommand(label) {
      var original = this.commandAliases[label];
      if (original) {
          this.commands[original].aliases.splice(this.commands[original].aliases.indexOf(label), 1);
          this.commandAliases[label] = undefined;
      } else {
          this.commands[label] = undefined;
      }
  }

  checkPrefix(msg) {
      var prefixes = this.commandOptions.prefix;
      if (typeof prefixes === "string") {
          return msg.content.replace(/<@/g, "<@").startsWith(prefixes) && prefixes;
      } else if (Array.isArray(prefixes)) {
          return prefixes.find((prefix) => msg.content.replace(/<@/g, "<@").startsWith(prefix));
      }
      throw new Error("Unsupported prefix format | " + prefixes);
  }
};

module.exports = CommandHandler;
