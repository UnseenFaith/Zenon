"use strict";
const pl = require('./PermissionLevel.js');
const json = require('jsonfile')

class Command {

  constructor(label, generator, options) {
      this.label = label;
      this.description = options.description || "No description";
      this.fullDescription = options.fullDescription || "No full description";
      this.usage = options.usage || "";
      this.aliases = options.aliases || [];
      this.caseInsensitive = !!options.caseInsensitive;
      this.deleteCommand = !!options.deleteCommand;
      this.guildOnly = !!options.guildOnly;
      this.dmOnly = !!options.dmOnly;
      this.level = options.level || 0;
      if (typeof generator === "string") {
          this.response = generator;
          this.execute = () => this.response;
      } else if (Array.isArray(generator)) {
          this.responses = generator.map((item, index) => {
              if (typeof generator === "string") {
                  return () => generator;
              } else if (typeof generator === "function") {
                  return generator;
              } else {
                  throw new Error("Invalid command response generator (index " + index + ")");
              }
          });
          this.execute = () => this.responses[Math.floor(Math.random() * this.responses.length)];
      } else if (typeof generator === "function") {
          this.execute = generator;
      } else {
          throw new Error("Invalid command response generator");
      }
      this.subcommands = {};
      this.subcommandAliases = {};
  }

  permissionCheck(msg) {
      var req = false;
      if (this.level > 0) {
          req = true;
          if (pl(msg) >= this.level) {
              return true;
          } else {
              msg.channel.sendMessage(`You do not have permission to use **${this.label}** ${msg.author.username}`);
              return false;
          }
      } else {
          return true;
      }
  }

process(args, msg) {
    if (!this.permissionCheck(msg)) {
        return;
    }
    if (args.length === 0) {
        return this.execute(msg, args);
    }
    var label = this.subcommandAliases[args[0]] || args[0];
      var subcommand;
      if((subcommand = this.subcommands[label]) !== undefined || ((subcommand = this.subcommands[label.toLowerCase()]) !== undefined && subcommand.caseInsensitive)) {
          return subcommand.process(args.slice(1), msg);
        }
    return this.execute(msg, args);
  }


registerSubcommand(label, generator, options) {
    if(label.includes(" ")) {
        throw new Error("Subcommand label may not have spaces");
    }
    if(this.subcommands[label]) {
        throw new Error("You have already registered a subcommand for " + label);
    }
    options = options || {};
    this.subcommands[label] = new Command(label, generator, options);
    if(options.aliases) {
        options.aliases.forEach((alias) => {
            this.subcommandAliases[alias] = label;
        });
    }
    return this.subcommands[label];
}

unregisterSubcommand(label) {
    var original = this.subcommandAliases[label];
    if(original) {
        this.subcommands[original].aliases.splice(this.subcommands[original].aliases.indexOf(label), 1);
        this.subcommandAliases[label] = undefined;
    } else {
        this.subcommands[label] = undefined;
    }
}

registerSubcommandAlias(alias, label) {
    if(!this.subcommands[label]) {
        throw new Error("No subcommand registered for " + label);
    }
    if(this.subcommandAliases[alias]) {
        throw new Error(`Alias ${label} already registered`);
    }
    this.subcommandAliases[alias] = label;
    this.subcommands[label].aliases.push(alias);
}

};

module.exports = Command;
