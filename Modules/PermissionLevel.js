var permissionLevel = {
    DEFAULT: 0,
    DJ: 1,
    MODERATOR: 2,
    ADMIN: 3,
    SUPPORT: 4,
    OWNER: 5,
    BOTOWNER: 10
};

module.exports = function (msg) {
    var roles = msg.guild.roles;
    var moderator = roles.find('name', 'Moderator');
    var admin = roles.find('name', 'Administrator');
    var dj = roles.find('name', 'DJ');
    if (msg.author.id === "105103261333196800")
        return permissionLevel.BOTOWNER;
    if (msg.author.id === "145733446491176960")
	    return permissionLevel.OWNER;
      if (admin !== null && msg.member.roles.has(admin.id) && msg.author.id != '105105022857056256')
        return permissionLevel.ADMIN;
    if (moderator !== null && msg.member.roles.has(moderator.id) && msg.author.id != '105105022857056256')
      return permissionLevel.MODERATOR;
    if (dj !== null && msg.member.roles.has(dj.id) && msg.author.id != '105105022857056256')
      return permissionLevel.DJ;
    return permissionLevel.DEFAULT;
};
