const ytdl = require("ytdl-core");
const YouTube = require('youtube-node');
const youtube = new YouTube();
const request = require('request');
const Constants = require('../mysecretkeys.js').keys;
const youtubeKey = Constants.youtubekey
youtube.setKey(Constants.youtubekey);

const djs = require('../app.js').djs;

class Music {
	constructor(stream) {
		this.stream = {
			connection: undefined,
			dispatcher: undefined,
			queue: [],
			skips: [],
			channel: undefined,
			playing: false,
			paused: false,
			autoplay: false,
			volume: 20
		};
		if (typeof stream === "object") {
			for (var property of Object.keys(stream)) {
				this.stream[property] = stream[property];
			};
		};
	};

	static search(content, msg) {
	  return new Promise(function(resolve, reject) {
			var Music = msg.guild.voiceConnection.music;
			Music.stream.channel.sendMessage(`Searching for **${content}**...`).then(msg2 => {
			request({url: `https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=1&q=${content}&type=video&videoCaption=any&fields=items&key=${Constants.youtubekey}`, json: true}, function (error, response, body) {
				if (error) {
					msg2.delete();
					reject(error);
				} else {
					let id = body.items[0].id.videoId;
					let title = body.items[0].snippet.title;
					let url = `https://www.youtube.com/watch?v=${id}`;
					Music.stream.queue.push({url: url, title: title, user: msg.author.username});
					msg2.edit(`\`[Music]\` *${title}* added to queue.`);
					msg2.delete(15000);
					resolve(true);
				}
			});
		});
	});
}


	static play(msg) {
		var Music = msg.guild.voiceConnection.music;
		Music.stream.playing = true;
		Music.stream.dispatcher = Music.stream.connection.playStream(ytdl(Music.stream.queue[0].url, { audioonly: true }));
		Music.stream.dispatcher.setVolumeLogarithmic(Music.stream.volume / 100);
		msg.channel.sendMessage(`\`[Music]\` **${Music.stream.queue[0].title}** is now playing.`).then(msg2 => {
			Music.stream.dispatcher.once('end', () => {
				if (Music.stream.autoplay != true) {
					Music.stream.queue.shift();
				}
				Music.stream.skips = [];
				msg2.delete();
				Music.checkQueue(msg);
			});
		});
	}

 checkQueue(msg) {
		if (this === undefined) {
			return;
		} else if (this.stream.queue[0] === undefined) {
			this.stream.playing = false;
			msg.channel.sendMessage("`[Music]` Playback has stopped because the **Queue** is empty.");
			return;
		} else if (this.stream.autoplay === true) {
			Music.autoPlay(msg);
		} else {
			Music.play(msg);
			return;
		}
	}

	static playList(content, msg, number) {
		return new Promise(function (resolve, reject) {
			var Music = msg.guild.voiceConnection.music;
	      Music.stream.channel.sendMessage(`\`[Music]\` Processing Playlist: \`${content}\``).then(msg2 => {
				youtube.addParam('maxResults', number);
				youtube.getPlayListsItemsById(content, function(error, result) {
					if (error) {
					msg2.delete();
					reject(error);
				} else {
					let jc = JSON.parse(JSON.stringify(result, null, 1));
					var x = 0;
					while (x < number) {
						let arrayItems = jc.items;
						let id = arrayItems[x].contentDetails.videoId;
						let title = arrayItems[x].snippet.title;
						let url = `https://www.youtube.com/watch?v=${id}`;
						Music.stream.queue.push({url: url, title: title, user: msg.author.username});
						x++;
					}
					msg2.edit(`\`[Music]\` ${number} songs added to queue.`);
					msg2.delete(15000);
					resolve(true);
				}
				});
		});
	});
}

	static checks(msg) {
		if (msg.guild.voiceConnection === (undefined || null)) {
			msg.reply("Please initialize the bot before attempting to play music or use music commands.");
			return false;
		} else if (msg.guild.voiceConnection !== (undefined || null) && msg.guild.voiceConnection.channel.id != msg.member.voiceChannelID) {
			msg.reply("Please join my voice channel before attempting play music or use music commands.");
			return false;
		} else if (msg.guild.voiceConnection !== (undefined || null) && msg.guild.voiceConnection.music.stream.autoplay === true && !(msg.content.includes('/skip') || msg.content.includes('/cancel'))) {
			msg.reply("You can not queue up music while autoplay is enabled.");
			return false;
		} else {
			return true;
		}
	};

	static getURL(id, msg) {
		return new Promise(function (resolve, reject) {
			var Music = msg.guild.voiceConnection.music;
			Music.stream.channel.sendMessage(`\`[Music]\` Processing ID: \`${id}\``).then((msg2) => {
				request({
					url: `https://www.googleapis.com/youtube/v3/videos?part=snippet&id=${id}&maxResults=1&key=${youtubeKey}`,
					json: true
				}, function(error, response, body) {
					if (error) {
					msg2.delete();
					reject(error);
				} else {
					let title = body.items[0].snippet.title;
					let url = `https://www.youtube.com/watch?v=${id}`;
					Music.stream.queue.push({
						url: url,
						title: title,
						user: msg.author.username
					})
					msg2.edit(`\`[Music]\` *${title}* added to queue.`);
					msg2.delete(10000);
					resolve(true);
				}
				});
			});
		});
	}

	static autoPlay(msg) {
		var queue = msg.guild.voiceConnection.music.stream.queue;
		var id = queue[0].url.toString().replace('https://www.youtube.com/watch?v=', '');
		request({
			url: `https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=1&relatedToVideoId=${id}&type=video&key=${youtubeKey}`,
			json: true
		}, function(error, response, body) {
			if (error) return console.log(error);
			let title = body.items[0].snippet.title;
			var id = body.items[0].id.videoId;
			var url = `https://www.youtube.com/watch?v=${id}`;
			queue.push({
				url: url,
				title: title
			});
			queue.shift();
		})
		setTimeout(() => {
			Music.play(msg)
		}, 1250);
	}
};

module.exports = Music;
