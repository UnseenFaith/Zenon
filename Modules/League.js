const djs = require('../app.js').djs;
const request = require('request');
const Constants = require('../mysecretkeys.js').keys;
const key = Constants.riotkey;

class League {
  static getSummonerByName(summonerName, region) {
      return new Promise(function(fulfill, reject) {
          request({
              url: `https://${region}.api.pvp.net/api/lol/${region}/v1.4/summoner/by-name/${summonerName}?api_key=${key}`,
              json: true
          }, function(error, response, body) {
              if (error) reject(error);
              else fulfill(body);
          });
      });
  }

  static getRankedByID(summonerID, region) {
      return new Promise(function(fulfill, reject) {
          request({
              url: `https://${region}.api.pvp.net/api/lol/${region}/v2.5/league/by-summoner/${summonerID}/entry?api_key=${key}`,
              json: true
          }, function(error, response, body) {
              if (error) reject(error);
              else fulfill(body);
          });
      });
  }

}

module.exports = League;
