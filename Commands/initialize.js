const Music = require('../Modules/Music.js');

exports.command = function(djs, cH) {
	cH.registerCommand("initalize", function(msg, args) {
			try {
				if (msg.guild.voiceConnection != undefined) {
					msg.reply(`There is already a voice connection on your server in **${msg.guild.voiceConnection.channel.name}**`);
				} else {
						if (msg.member.voiceChannel === undefined) {
							msg.reply("You must be in a voice channel to use this command!");
							return;
						} else {
						msg.member.voiceChannel.join().then(conn => {
						msg.guild.voiceConnection.music = new Music({connection: conn, channel: msg.channel});
						});
						msg.channel.sendMessage(`\`[Music]\` Connection Initialized by **${msg.author.username}** Joining: **${msg.member.voiceChannel.name}**`);
				}
			}
		} catch (error) {
			djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
		};
	}, {
    aliases: ['init'],
		caseInsenitive: true,
		deleteCommand: true,
		description: 'Joins the given channel you are in.',
		fullDescription: 'This command will initalize a voice connection on the server you use the command in and the channel you\'re in.',
		dmOnly: false,
		guildOnly: false,
		level: 1
	});
};
