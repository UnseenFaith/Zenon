exports.command = function(djs, cH) {
	cH.registerCommand("destroy", function(msg, args) {
		try {
			if (msg.guild.voiceConnection == undefined) {
				msg.reply("There is no voice connection on your server.");
			} else {
				msg.channel.sendMessage(`\`[Music]\` Connection Destroyed by **${msg.author.username}**. Leaving: **${msg.guild.voiceConnection.channel.name}**`);
				msg.guild.voiceConnection.channel.leave();
			}
		} catch (error) {
			djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
		}
	}, {
		caseInsenitive: true,
		deleteCommand: true,
		description: 'Destroys the connection on the server it is used on.',
		fullDescription: 'This command will destroy a voice connection on any server, whilst maintaining connections on others servers.',
		dmOnly: false,
		guildOnly: false,
		level: 1
	});
};
