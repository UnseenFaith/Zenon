var pl = require("../Modules/PermissionLevel.js");

exports.command = function (djs, cH) {
cH.registerCommand("help", (msg, args) => {
    var result = "";
    if(args.length > 0) {
        var cur = cH.commands[cH.commandAliases[args[0]] || args[0]];
        if(!cur) {
            return "Command not found";
        }
        var label = cur.label;
        for(var i = 1; i < args.length; ++i) {
            cur = cur.subcommands[cur.subcommandAliases[args[i]] || args[i]];
            if(!cur) {
                return "Command not found";
            }
            label += " " + cur.label;
        }
        result += `**${msg.prefix}${label}** ${cur.usage}\n${cur.fullDescription}\n\n__Permission level required to use:__ **${cur.level}**`;
        if(Object.keys(cur.aliases).length > 0) {
            result += `\n\n**Aliases:** ${cur.aliases.join(", ")}`;
        }
        if(Object.keys(cur.subcommands).length > 0) {
            result += "\n\n**Subcommands:**";
            for(var subLabel in cur.subcommands) {
                if(cur.subcommands[subLabel].permissionCheck(msg)) {
                    result += `\n  **${subLabel}** - ${cur.subcommands[subLabel].description}`;
                }
            }
        }
    } else {
        result += `${cH.commandOptions.name} - ${cH.commandOptions.description}\n`;
        if(cH.commandOptions.owner) {
            result += `by ${cH.commandOptions.owner}\n`;
        }
        result += "\n";
        result += "**Commands:**\n";
        for(label in cH.commands) {
            if(cH.commands[label] && cH.commands[label].level <= pl(msg)) {
                result += `  **${msg.prefix}${label}** - ${cH.commands[label].description}\n`;
            }
        }
        result += `\nType ${msg.prefix}help <command> for more info on a command.`;
    }
    return result;
}, {
    description: "This help text",
    fullDescription: "This command is used to view information of different bot commands, including this one."
});
};
