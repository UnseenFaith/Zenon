var jsonfile = require('jsonfile')
var tags = [];

exports.command = function (djs, cH) {
  cH.commands['tags'].registerSubcommand("start", function (msg, args) {
    try {
      jsonfile.readFile(`Commands/tags/data/${msg.guild.id}.json`, function (err, object) {
        if (err) {
          jsonfile.writeFile(`Commands/tags/data/${msg.guild.id}.json`, tags, {spaces: 2}, function (err) {
      });
    } else {
      msg.reply("There is already a tags data file for your server.");
    }
  });
    } catch (error) {
      djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
    };
    }, {
      caseInsensitive: true,
      deleteCommand: true,
      description: 'Creates a tag data file for your server.',
      fullDescription: 'This command will create a data file for your server that contains all of the tags that your server has. Ask a Staff member to initiate this command first before trying to create, edit, or delete tags.',
      dmOnly: false,
      guildOnly: false,
      level: 5
    });
}
