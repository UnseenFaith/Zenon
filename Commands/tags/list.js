var jsonfile = require('jsonfile');

exports.command = function(djs, cH) {
	cH.commands['tags'].registerSubcommand("list", function(msg, args) {
		try {
			jsonfile.readFile(`Commands/tags/data/${msg.guild.id}.json`, function(err, tags) {
				if (err) {
					msg.reply("A staff member has not created the tag data file yet. Ask one to create it for you.");
					return;
				} else {
          var result = "";
          tags.forEach(function(tag, index) {
						if (index === tags.length - 1) {
            result += `\`${tag.name}\``;
					} else {
						result += `\`${tag.name}\` | `;
					}
          });
          msg.channel.sendMessage(`Here a list of the current tags on your server.\n\n${result}`);
				}
			});
		} catch (error) {
			djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
		};
	}, {
		caseInsensitive: true,
		deleteCommand: true,
		description: 'Displays all tags for your server.',
		fullDescription: 'This command will display all tags on your server, which you can call back using the tag command.',
		dmOnly: false,
		guildOnly: false,
		level: 0
	});
}
