var jsonfile = require('jsonfile');

exports.command = function(djs, cH) {
	cH.commands['tags'].registerSubcommand("delete", function(msg, args) {
		try {
			if (args[0] === undefined) {
				msg.reply(`Improper use of the command.\nProper usage: \`${cH.commandOptions.prefix}${cH.commands['tags'].label} ${cH.commands['tags'].subcommands['create'].label} ${cH.commands['tags'].subcommands['create'].usage}\``)
				return;
			}
			jsonfile.readFile(`Commands/tags/data/${msg.guild.id}.json`, function(err, tags) {
				if (err) {
					msg.reply("A staff member has not created the tag data file yet. Ask one to create it for you.");
					return;
				} else {
					var name = args[0];
					var filtered = tags.filter(function(tag) {
						return tag.name === name
					});
					if (filtered[0] === undefined) {
						msg.reply("There is no tag with that name.");
						return;
					} else if (filtered[0].author != msg.author.id) {
            msg.reply("You cannot delete a tag you didn't make");
            return;
          }
          var index = tags.findIndex(t => t.name === name);
          var newtags = tags.splice(index, 1);
					jsonfile.writeFile(`Commands/tags/data/${msg.guild.id}.json`, tags, {
						spaces: 2
					}, function(err) {
						msg.channel.sendMessage(`**${msg.author.username}** deleted the tag \`${name}\` `);
            return;
					});
				}
			});
		} catch (error) {
			djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
		};
	}, {
		caseInsensitive: true,
		deleteCommand: true,
		description: 'Deletes a tag for your server.',
		fullDescription: 'This command will dleete a tag with a given name on your server.',
		dmOnly: false,
		guildOnly: false,
		usage: 'namehere ',
		level: 0
	});
}
