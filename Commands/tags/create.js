var jsonfile = require('jsonfile');

exports.command = function(djs, cH) {
	cH.commands['tags'].registerSubcommand("create", function(msg, args) {
		try {
			if ((args[0] || args[1]) === undefined) {
				msg.reply(`Improper use of the command.\nProper usage: \`${cH.commandOptions.prefix}${cH.commands['tags'].label} ${cH.commands['tags'].subcommands['create'].label} ${cH.commands['tags'].subcommands['create'].usage}\``)
				return;
			}
			if (args[0].includes(" ")) {
				msg.reply("Tag names cannot have spaces in them.");
				return;
			}
			jsonfile.readFile(`Commands/tags/data/${msg.guild.id}.json`, function(err, tags) {
				if (err) {
					msg.reply("A staff member has not created the tag data file yet. Ask one to create it for you.");
					return;
				} else {
					let name = args[0];
					let description = args.slice(1).join(' ');
					let author = msg.author.id;
					var filtered = tags.filter(function(tag) {
						return tag.name === name
					});
					if (filtered[0] != undefined) {
						msg.reply("There is already a tag with that name.");
						return;
					}
					tags.push({
						name: name,
						description: description,
						author: author
					});
					jsonfile.writeFile(`Commands/tags/data/${msg.guild.id}.json`, tags, {
						spaces: 2
					}, function(err) {
						msg.channel.sendMessage(`**${msg.author.username}** created the tag \`${name}\` with the description:\n${description}`);
					});
				}
			});
		} catch (error) {
			djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
		};
	}, {
		caseInsensitive: true,
		deleteCommand: true,
		description: 'Creates a tag for your server.',
		fullDescription: 'This command will create a tag with a given name and given description, then you call back the tag by using the tags command with the name after it.',
		dmOnly: false,
		guildOnly: false,
		usage: 'namehere descriptionhere',
		level: 0
	});
}
