exports.command = function(djs, cH) {
  cH.registerCommand("inviteme", function (msg, args) {
    try {
    msg.channel.sendMessage("So you'd like to invite me to another server that you're on? You can do so by using this link: \n<https://discordapp.com/oauth2/authorize?client_id=209423894464036874&scope=bot&permissions=0> \nYou'll need the `Manage Server` permission for this to work.");
  } catch (error) {
    djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
  }
}, {
  caseInsensitive: true,
  deleteCommand: true,
  description: 'Returns a link in chat that you can use to invite the bot.',
  fullDescription: 'This command will send a link in chat that you can use to invite the bot to a server you have the `Manage Server` permission on. Note that not all features will work if the bot does not have permission, such as deleting other users messages or joining voice channels.',
  level: 0
});
}
