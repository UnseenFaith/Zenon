const Music = require('../Modules/Music.js');

exports.command = function (djs, cH) {
cH.registerCommand("volume", function (msg, args) {
  try {
    if (Music.checks(msg)) {
      var Stream = msg.guild.voiceConnection.music.stream;
      if (parseInt(args[0]) > 100 || parseInt(args[0]) === NaN) {
      msg.reply("You either did not type a number or you typed a number over 100. Refer to the usage for the volume command if you're confused.");
      return;
    } else {
      let volume = parseInt(args[0]);
      Stream.volume = volume;
      Stream.dispatcher.setVolumeLogarithmic(volume / 100);
      msg.channel.sendMessage(`\`[Music]\` Logarithmic Volume changed to ${volume}% by: **${msg.author.username}**`);
    }
  }
  } catch (error) {
    djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
  }
}, {
  caseInsensitive: true,
  deleteCommand: true,
  description: 'Changes the volume of the playback and future songs.',
  fullDescription: 'This command will change the volume of the current song and any future songs played on the server you\'re on.',
  dmOnly: false,
  guildOnly: false,
  level: 1,
  usage: '10'
});
};
