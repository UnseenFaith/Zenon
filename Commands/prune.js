exports.command = function(djs, cH) {
    cH.registerCommand("prune", function(msg, args) {
        try {
                const params = args;
                let mentions = msg.mentions.users.array();
                if (mentions[0] === undefined) {
                    var mention = djs.user.id;
                    var messageCount = parseInt(params[0]);
                } else {
                    var mention = mentions[0].id;
                    var messageCount = parseInt(params[1]);
                }
                msg.channel.fetchMessages({
                        limit: 100
                    })
                    .then(messages => {
                        let msg_array = messages.array();
                        msg_array = msg_array.filter(m => m.author.id === mention);
                        msg_array.length = messageCount;
                        msg_array.map(m => m.delete().catch(console.error));
                    });
        } catch (error) {
            console.log(error);
        }
    }, {
        aliases: ["purge"],
        caseInsensitive: true,
        deleteCommand: true,
        description: 'Removes messages sent by the bot or a mentioned user.',
        fullDescription: 'This command will delete any messages (up to 100) sent by any mentioned user or the bot.',
        dmOnly: false,
        guildOnly: true,
        usage: '1 **or** @mention 1' ,
        level: 3
    });
}
