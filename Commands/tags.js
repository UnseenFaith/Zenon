var pl = require('../Modules/PermissionLevel.js');
var jsonfile = require('jsonfile');

exports.command = function (djs, cH) {
  cH.registerCommand("tags", function (msg, args) {
    try {
      if (args[0] === undefined) {
      msg.channel.sendMessage("So you're wondering what tags are? Tags are a way to store information (up to 1000 characters) and easily recall it later using a command. This creates an easy to use command interface for displaying information you want to save.");
    } else {
      jsonfile.readFile(`Commands/tags/data/${msg.guild.id}.json`, function(err, tags) {
        if (err) {
          msg.reply("A staff member has not created the tag data file yet. Ask one to create it for you.");
          return;
        } else {
          let name = args[0];
          var filtered = tags.filter(function(tag) {
            return tag.name === name
          });
          if (filtered[0] === undefined) {
            msg.reply("There is no tag with that name.");
            return;
          }
          msg.channel.sendMessage(`Tag \`${filtered[0].name}\`\n${filtered[0].description}`)
        }
      });
    }
    } catch (error) {
      djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
    };
    }, {
      aliases: ['tag'],
      caseInsensitive: true,
      deleteCommand: true,
      description: 'Returns information on tags.',
      fullDescription: 'This command will return information on tags, and the commands below will help you in using them.',
      dmOnly: false,
      guildOnly: false,
      level: 0,
      usage: 'tagnamehere'
    });
}
