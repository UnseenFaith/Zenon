var beautify = require('js-beautify').js_beautify;
var options = {
    "indent_size": 4,
    "indent_char": " ",
    "eol": "\n",
    "indent_level": 0,
    "indent_with_tabs": false,
    "preserve_newlines": true,
    "max_preserve_newlines": 10,
    "jslint_happy": false,
    "space_after_anon_function": false,
    "brace_style": "collapse",
    "keep_array_indentation": false,
    "keep_function_indentation": false,
    "space_before_conditional": true,
    "break_chained_methods": false,
    "eval_code": false,
    "unescape_strings": false,
    "wrap_line_length": 0,
    "wrap_attributes": "auto",
    "wrap_attributes_indent_size": 4,
    "end_with_newline": false
}

exports.command = function(djs, cH) {
    cH.registerCommand("beautify", function(msg, args) {
        let code = args.join(' ');
        try {
            msg.channel.sendCode('js', beautify(code.replace(/```/g, "XXX").replace(/`/g, "\`"), {
                options
            }));
        } catch (error) {
            djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
        }
    }, {
        aliases: ['bt', 'beauty'],
        caseInsensitive: true,
        deleteCommand: true,
        description: 'Beautifies code.',
        fullDescription: 'This command will beautify any given code into JavaScript syntax. Will be expanded later to support other languages.',
        dmOnly: false,
        guildOnly: false,
        usage: 'codehere',
        level: 4
    });
};
