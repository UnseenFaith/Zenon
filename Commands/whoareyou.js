var StringBuilder = require('stringbuilder');
var package = require('../package.json');
exports.command = function (djs, cH) {
  cH.registerCommand("whoareyou", function (msg, args) {
    try {
    var sb = new StringBuilder({
      newline: '\r\n'
    });
    sb.appendLine("Name:       " + djs.user.username);
    sb.appendLine("Nickname:   " + (msg.guild.member(djs.user).nickname === (null || undefined) ? "No Nickname" : msg.guild.member(djs.user).nickname));
    sb.appendLine("Version:    " + package.version);
    sb.appendLine("ID:         " + djs.user.id);
    sb.appendLine("Channels:   " + djs.channels.size);
    sb.appendLine("Guilds:     " + djs.guilds.size);
    sb.appendLine("Users:      " + djs.users.size);
    sb.appendLine("Uptime:     " + djs.uptime);
    sb.appendLine("Joined:     " + msg.guild.member(djs.user).joinedAt);
    sb.appendLine("Avatar:     " + djs.user.avatarURL);
    sb.build(function (err, result) {
      msg.channel.sendCode('ruby', result);
    });
  } catch (error) {
    djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
  };
  }, {
    aliases: ["way"],
    caseInsensitive: true,
    deleteCommand: true,
    description: 'Returns information on the bot.',
    fullDescription: 'This command will give you information about the bot on the given server that you use it on. Some information will change from server to server.',
    dmOnly: false,
    guildOnly: false,
    level: 0
  });
};
