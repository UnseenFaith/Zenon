exports.command = function(djs, cH) {
    cH.registerCommand("mute", function(msg, args) {
        try {
            if (args[0] === undefined) {
                msg.reply("Invalid use of the command. Please refer to the usage of the command using the help command.");
                return;
            } else {
                var mentions = msg.mentions.users.array();
                var id = mentions[0].id;
                if (id === undefined) {
                    msg.reply("You did not mention anyway to temporarily mute.");
                    return;
                } else {
                    msg.channel.overwritePermissions(id, {
                        SEND_MESSAGES: false
                    });
                    msg.channel.sendMessage(`**${mentions[0].username}** has been permanently muted by **${msg.author.username}**`);
                }
            }
        } catch (error) {
            djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
        }
    }, {
        caseInsensitive: true,
        deleteCommand: true,
        description: 'Permanently mutes a user.',
        fullDescription: 'This command will permanently mutes a mentioned user.',
        usage: '@mention',
        level: 2
    });
}
