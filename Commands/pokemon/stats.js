var myCache = require('../../app.js').myCache;
var request = require('request');

exports.command = function (djs, cH) {
  cH.commands['pokemon'].registerSubcommand("stats", function (msg, args) {
    try {
      if (args[0] === undefined) {
        msg.reply("Please specify a pokemon to get stats for.");
        return;
      }
      var pokemon = args[0].toLowerCase();
      myCache.get(pokemon, function (err, value) {
        if(!err) {
          if(value != undefined) {
            var name = value.name;
            var national_id = value.national_id;
            var hp = value.hp;
            var atk = value.attack;
            var def = value.defense;
            var spatk = value.sp_atk;
            var spdef = value.sp_def;
            var speed = value.speed;
            msg.channel.sendMessage(`**${name}**, National Dex ID: **${national_id}**\n__Base Stats__\nHP - ${hp}\nATK - ${atk}\nDEF - ${def}\nSPATK - ${spatk}\nSPDEF - ${spdef}\nSPEED - ${speed}`);
          } else {
            request({url: `https://pokeapi.co/api/v1/pokemon/${pokemon}/`, json: true}, function (error, response, value) {
              myCache.set(pokemon, value, function (err, success) {
                if (!err && success) {
                  console.log("Item Cached");
                }
              });
              if (value === undefined) {
                msg.reply("That is not a valid pokemon name or national dex number.");
              }
              var name = value.name;
              var national_id = value.national_id;
              var hp = value.hp;
              var atk = value.attack;
              var def = value.defense;
              var spatk = value.sp_atk;
              var spdef = value.sp_def;
              var speed = value.speed;
              msg.channel.sendMessage(`**${name}**, National Dex ID: **${national_id}**\n__Base Stats__\nHP - ${hp}\nATK - ${atk}\nDEF - ${def}\nSPATK - ${spatk}\nSPDEF - ${spdef}\nSPEED - ${speed}`);
        });
      }
    }
  });
    } catch (error) {
      djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
    }
  })
}
