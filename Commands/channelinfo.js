var StringBuilder = require('stringbuilder');

exports.command = function(djs, cH) {
    cH.registerCommand("channelinfo", function(msg, args) {
        try {
            var name = args.join(' ');
            if (name === undefined) {
              msg.reply("Improper use of the command.");
              return;
            }
            var channel = msg.guild.channels.find('name', name);
            if (channel === undefined) {
              msg.reply("That channel does not exist on this guild.");
              return;
            }
            var sb = new StringBuilder({
                newline: '\r\n'
            });
            sb.appendLine('**Channel Information:  **');
            sb.appendLine("```ruby");
            sb.appendLine("Name:          " + channel.name.toUpperCase());
            sb.appendLine("ID:            " + channel.id);
            sb.appendLine("Type:          " + channel.type.toUpperCase());
            sb.appendLine("Position:      " + channel.position);
            sb.appendLine("Members:       " + channel.members.size);
            if (channel.type === 'voice') {
              sb.appendLine("Bit Rate:      " + channel.bitrate);
              sb.appendLine("User Limit:    " + channel.userLimit)
            }
            if (channel.type === 'text') {
              sb.appendLine("Topic:         " + channel.topic);
            }
            sb.appendLine("Created at:    " + channel.createdAt);
            sb.appendLine("```");
            sb.build(function(err, result) {
                msg.channel.sendMessage(result);
            });
        } catch (err) {
            console.log(err);
        }
    }, {
        aliases: ['chaninfo'],
        caseInsensitive: true,
        deleteCommand: true,
        description: 'Returns information on the channel of a server/guild you\'re in',
        fullDescription: 'This command will return a ton of information related to the channel on the server/guild you use the command in.',
        level: 0
    });
}
