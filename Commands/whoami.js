var StringBuilder = require('stringbuilder');

exports.command = function (djs, cH) {
  cH.registerCommand("whoami", function (msg, args) {
    try {
    var sb = new StringBuilder({
      newline: '\r\n'
    });
    var roles = msg.member.roles.array();
    var role = [];
    var x = 0;
    while (x < roles.length) {
      role.push(roles[x].name);
      x++;
    }
    role.shift();
    sb.appendLine("Name:       " + msg.author.username + "#" + msg.author.discriminator);
    sb.appendLine("Nickname:   " + (msg.member.nickname === (null || undefined) ? "No Nickname" : msg.member.nickname));
    sb.appendLine("ID:         " + msg.author.id);
    sb.appendLine("Bot:        " + msg.author.bot);
    sb.appendLine("Roles:      [" + role.length + "] " + role.join(', '));
    sb.appendLine("Status:     " + msg.author.presence.status.toUpperCase());
    sb.appendLine("Game:       " + (msg.author.presence.game === null ? "Nothing" : msg.author.presence.game.name));
    sb.appendLine("Created:    " + msg.author.createdAt);
    sb.appendLine("Joined:     " + msg.member.joinedAt);
    sb.appendLine("Avatar:     " + msg.author.avatarURL);
    sb.build(function (err, result) {
      msg.channel.sendCode('ruby', result);
    });
  } catch (error) {
    djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
  };
  }, {
    aliases: ["wai"],
    caseInsensitive: true,
    deleteCommand: true,
    description: 'Returns information on yourself.',
    fullDescription: 'This command will give you information about yourself on the given server that you use it on. Some information will change from server to server.',
    dmOnly: false,
    guildOnly: false,
    level: 0
  });
};
