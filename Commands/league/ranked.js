const League = require('../../Modules/League.js');
exports.command = function(djs, cH) {
cH.commands['league'].registerSubcommand("ranked", function (msg, args) {
  try {
    if (args[0] === undefined) {
      msg.reply("Please specify a league name to search for.");
      return;
    }
    var summonerName = args.join('%20').toLowerCase();
    var region = 'na';
    var summoner = summonerName.replace(/%20/g, '');
    League.getSummonerByName(summonerName, region).then(body => {
      if (body.status === undefined) {
          var summObject = body[summoner];
          League.getRankedByID(summObject.id, region).then(body2 => {
            let ranked = body2[summObject.id].filter(function (rank) { return rank.queue === 'RANKED_SOLO_5x5'});
            let winRatio = ranked[0].entries[0].wins / (ranked[0].entries[0].wins + ranked[0].entries[0].losses);
            msg.channel.sendMessage(`**${summObject.name}** - Ranked 5v5 Information\n**League Name:** ${ranked[0].name}\n**League Tier/Division:** ${ranked[0].tier} ${ranked[0].entries[0].division}\n**League Points:** ${ranked[0].entries[0].leaguePoints}\n**Playstyle:** ${ranked[0].entries[0].playstyle}\n**Wins:** ${ranked[0].entries[0].wins}\n**Losses:** ${ranked[0].entries[0].losses}\n**Win Ratio:** ${winRatio.toFixed(2)}`);
          });
        } else {
          msg.channel.sendMessage(`**Status Code:** ${body.status.status_code}\n**Message:** ${body.status.message}`);
        }
    });
  } catch (error) {
    djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
  }
}, {
  caseInsensitive: true,
  deleteCommand: true,
  description: 'Returns Ranked information about a summoner.',
  fullDescription: 'This command will return ranked information on a region. Defaults to the North American server. If you want to search on a different server, simply append a new region code on the end of the command.\n__Region Codes:__ **br, eune, euw, na, jp, kr, lan, las, oce, ru, tr**',
  usage: 'l3ap of faith **or** wickd EUW',
  level: 0
})
}
