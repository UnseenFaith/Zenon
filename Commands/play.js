const Music = require('../Modules/Music.js');

exports.command = function (djs, cH) {
  cH.registerCommand("play", function (msg, args) {
    try {
      var content = args.join(' ');
      if (Music.checks(msg)) {
          Music.search(content, msg)
              .then(response => {
                  if (msg.guild.voiceConnection.music.stream.playing === false) {
                      Music.play(msg);
                  } else {
                      return;
                  };
              })
              .catch(error => {
                  msg.channel.sendMessage(error);
              });
      };
  } catch (error) {
    djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
  }
}, {
  caseInsensitive: true,
  deleteCommand: true,
  description: 'Plays music by searching youtube for terms you specify.',
  fullDescription: 'This command will search youtube and play (or add to queue) songs you request it to add. If you have a URL or playlist you would like to add, use one of the latter commands mentioned below.',
  dmOnly: false,
  guildOnly: false,
  level: 0,
  usage: 'monstercat roses'
});
};
