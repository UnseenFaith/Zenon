const Music = require('../Modules/Music.js');

exports.command = function (djs, cH) {
  cH.registerCommand("queue", function (msg, args) {
    try {
      if (Music.checks(msg)) {
        var Stream = msg.guild.voiceConnection.music.stream;
        if (Stream.autoplay === true) {
        msg.reply("You can't use the queue command while autoplay is active.");
        return;
      } else if (Stream.queue[0] != undefined) {
        var x = 1;
        var result = "**__Current Queue:__**"
        result += `\n[Now Playing] **${Stream.queue[0].title}** added by - ${Stream.queue[0].user}`;
        while (x < Stream.queue.length && x <= 9) {
          result += `\n[${x}] **${Stream.queue[`${x}`].title}** added by - __${Stream.queue[0].user}__`;
          x++;
        }
        if (Stream.queue.length > 9) {
          result += `... and __${Stream.queue.length - 9}__ more songs in queue.`;
        }
        msg.channel.sendMessage(result);
      } else {
        msg.reply("There is nothing in queue to display");
        return;
      }
    }
    } catch (error) {
      djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
    }
  }, {
    caseInsensitive: true,
    deleteCommand: true,
    description: 'Displays the current queue for oyu server.',
    fullDescription: 'This command will display the current queue of songs for your server, up to a maximum of 10 songs, with a message that tells you how many more songs are in the queue.',
    dmOnly: false,
    guildOnly: false,
    level: 0
  });
};
