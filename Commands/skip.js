const pl = require("../Modules/PermissionLevel.js");
const Music = require('../Modules/Music.js');

exports.command = function(djs, cH) {
	cH.registerCommand("skip", function(msg, args) {
		try {
			if (Music.checks(msg)) {
				var Stream = msg.guild.voiceConnection.music.stream;
				if (Stream.playing === false) {
					msg.reply("Nothing is currently playing.");
				} else {
					if (pl(msg) >= 5) {
						Stream.dispatcher.end();
						msg.channel.sendMessage(`\`[Music]\` Song forcefully skipped by: **${msg.author.username}**`);
						return;
					} else if (Stream.skips.includes(msg.author.id)) {
						msg.reply("Stop trying to cheat the system, you've already skipped once. :eyes:");
						return;
					} else {
						Stream.skips.push(msg.author.id);
						var skip = (Math.floor((msg.member.voiceChannel.members.array().length) / 2));
						if (Stream.skips.length < skip) {
							msg.channel.sendMessage(`\`[Music]\` A Vote for skipping has been put in place. Votes left to skip: **${(skip - Stream.skips.length)}**`);
							return;
						} else {
							msg.channel.sendMessage("`[Music]` Skip vote has been successful! Skipping");
							Stream.dispatcher.end();
							return;
						}
					}
				}
			}
		} catch (error) {
			djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
		}
	}, {
		caseInsensitive: true,
		deleteCommand: true,
		description: 'Starts a vote skip or skips the song forcefully depending on permission level.',
		fullDescription: 'This command will start a vote skip for the song that you are currently on, or if you have a high enough permission level, will automatically skip to the next song.',
		level: 0
	});
};
