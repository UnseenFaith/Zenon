const Music = require('../Modules/Music.js');

exports.command = function (djs, cH) {
  cH.registerCommand("cancel", function (msg, args) {
    try {
    if (Music.checks(msg)) {
      if (msg.guild.voiceConnection.music.stream.autoplay === false) {
      msg.reply("Autoplay is not currently enabled.");
      return;
    } else {
      msg.guild.voiceConnection.music.stream.autoplay = false;
      msg.channel.sendMessage(`\`[Music]\` Autoplay disabled.`);
      return;
    }
  }
  } catch (error) {
    djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
  }
}, {
  caseInsensitive: true,
  deleteCommand: true,
  description: 'Stops the current autoplay and re-enables queueing up songs and the queue command.',
  fullDescription: 'This command will stop the current autoplay session and return to using the queue and queued up songs after the current song is over.',
  level: 1
})
}
