const StringBuilder = require('stringbuilder');
const util = require('util');
const pl = require('../Modules/PermissionLevel.js');

exports.command = function (djs, cH) {
  cH.registerCommand("evaluate", function (msg, args) {
          try {
          var content = args.join(' ');
          var sb = new StringBuilder({
              newline: '\r\n'
          });
          var evalContent = eval(content);
          sb.appendLine('`INPUT:`');
          sb.appendLine("```js");
          sb.appendLine('{0}', content);
          sb.appendLine("```");
          sb.appendLine('`OUTPUT:`');
          sb.appendLine('```js');
          sb.appendLine('{0}', evalContent);
          sb.appendLine('```');
        if (typeof evalContent === 'object' && evalContent != (undefined || null)) {
              sb.appendLine('`INSPECT:`');
              sb.appendLine('```js');
              sb.appendLine('{0}', util.inspect(evalContent, false, 0, false));
              sb.appendLine('```');
          };
          sb.build(function (err, result) {
              msg.channel.sendMessage(result);
          });
      } catch (err) {
          var sb = new StringBuilder({
              newline: '\r\n'
          });
          sb.appendLine('`INPUT:`');
          sb.appendLine('```js');
          sb.appendLine('{0}', content);
          sb.appendLine('```');
          sb.appendLine('`ERROR:`');
          sb.appendLine('```js');
          sb.appendLine('{0}', err);
          sb.appendLine('```');
          sb.build(function (err, result) {
              msg.channel.sendMessage(result);
          });
      };
  }, {
      aliases: ['eval'],
      caseInsensitive: true,
      deleteCommand: true,
      description: 'Evaluates given expressions.',
      fullDescription: 'This command will evaluate any given expression that you give it. Only usable by trusted users',
      dmOnly: false,
      guildOnly: false,
      usage: 'msg.author.id',
      level: 5,
      usage: `msg.author.id`
  });
};
