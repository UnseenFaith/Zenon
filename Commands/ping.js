exports.command = function(djs, cH) {
    cH.registerCommand("ping", function(msg, args) {
        try {
          msg.channel.sendMessage("Pinging...").then(msg2 => {
            msg2.edit(`Pinging...`).then(msg3 => {
              msg3.edit(`Ping Received in: **${msg3.editedTimestamp - msg2.createdTimestamp} MS**`);
            });
          });
        } catch (error) {
            djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
        }
    }, {
        aliases: ['pong'],
        caseInsensitive: true,
        deleteCommand: true,
        description: 'Pings the bot to return a delay in MS.',
        fullDescription: 'This command will ping the bot and respond in how many milliseconds it took to process the command and return it.',
        level: 0
    });
}
