var request = require('request');
var util = require('util');
var myCache = require('../app.js').myCache;

exports.command = function(djs, cH) {
cH.registerCommand("pokemon", function (msg, args) {
  try {
    if (args[0] === undefined) {
      msg.reply("Please specify a pokemon's name to search for.");
      return;
    }
    var pokemon = args[0].toLowerCase();
    myCache.get(pokemon, function (err, value) {
      if(!err) {
        if(value != undefined) {
          var name = value.name;
          var national_id = value.national_id;
          var descriptions = value.descriptions;
          var index = Math.floor((Math.random() * descriptions.length));
          var resource = descriptions[index].resource_uri;
          request({url: `https://pokeapi.co/${resource}`, json: true}, function (error, response, body) {
            var description = util.format('%s', body.description.replace(/\n/g, ' ').replace(/POKMON/g, 'Pokémon').replace(/POKéMON/g, 'Pokémon'));
            msg.channel.sendMessage(`**${name}**, National Dex ID: **${national_id}**\n${description}`);
          });
        } else {
    request({url: `https://pokeapi.co/api/v1/pokemon/${pokemon}/`, json: true}, function (error, response, body) {
      myCache.set(pokemon, body, function (err, success) {
        if (!err && success) {
        }
      });
      if (body === undefined) {
        msg.reply("That is not a valid pokemon name or national dex number.");
        return;
      }
      var name = body.name;
      var national_id = body.national_id;
      var descriptions = body.descriptions;
      var index = Math.floor((Math.random() * descriptions.length));
      var resource = descriptions[index].resource_uri;
      request({url: `https://pokeapi.co/${resource}`, json: true}, function (error, response, body) {
        var description = util.format('%s', body.description.replace(/\n/g, ' ').replace(/POKMON/g, 'Pokémon').replace(/POKéMON/g, 'Pokémon'));
        msg.channel.sendMessage(`**${name}**, National Dex ID: **${national_id}**\n${description}`);
      })
    })
  }
}
});
  } catch (error) {
    djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
  }
}, {
  aliases: ['poke'],
  caseInsensitive: true,
  deleteCommand: true,
  description: 'Returns a random description about the pokemon.',
  fullDescription: 'This command will return a description about your pokemon, anywhere from Pokemon Red/Blue to Pokemon Omega Ruby/Alpha Sapphire',
  usage: 'pikachu',
  level: 0
})
}
