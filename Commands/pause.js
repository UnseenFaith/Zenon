const Music = require('../Modules/Music.js');

exports.command = function(djs, cH) {
	cH.registerCommand("pause", function(msg, args) {
		try {
			if (Music.checks(msg)) {
				var Stream = msg.guild.voiceConnection.music.stream;
				if (Stream.paused === false && Stream.playing === true) {
        Stream.paused = true;
				Stream.dispatcher.pause();
        msg.channel.sendMessage(`\`[Music]\`Playback has been paused by ${msg.author.username}`)
			} else if (Stream.paused === true || Stream.playing === false) {
        msg.reply("There is no music playing or the bot is already paused.")
      }
		}
		} catch (error) {
			djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
		}
	}, {
		caseInsenitive: true,
		deleteCommand: true,
		description: 'Pauses the current playback.',
		fullDescription: 'This command will pause any playback on your server, whilst maintaining connections on others servers.',
		dmOnly: false,
		guildOnly: false,
		level: 1
	});
};
