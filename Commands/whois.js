var StringBuilder = require('stringbuilder');
const pl = require('../Modules/PermissionLevel');

exports.command = function (djs, cH) {
  cH.registerCommand("whois", function (msg, args) {
    try {
    let mentions = msg.mentions.users.array();
    let id = (mentions[0] === undefined ? args[0] : mentions[0].id);
    let author = djs.users.find('id', id);
    let member = msg.guild.members.find('id', id);
    var roles = member.roles.array();
    var role = [];
    var x = 0;
    while (x < roles.length) {
      role.push(roles[x].name);
      x++;
    }
    role.shift();
    var sb = new StringBuilder({
      newline: '\r\n'
    });
    sb.appendLine("Name:       " + author.username + "#" + author.discriminator);
    sb.appendLine("Nickname:   " + ((member.nickname === null || member.nickname === undefined) ? "No Nickname" : member.nickname));
    sb.appendLine("ID:         " + author.id);
    sb.appendLine("Bot:        " + author.bot);
    sb.appendLine("Roles:      [" + role.length + "] " + role.join(', '));
    sb.appendLine("Status:     " + author.presence.status.toUpperCase());
    sb.appendLine("Game:       " + (author.presence.game === null ? "Nothing" : author.presence.game.name));
    sb.appendLine("Created:    " + author.createdAt);
    sb.appendLine("Joined:     " + member.joinedAt);
    sb.appendLine("Avatar:     " + author.avatarURL);
    sb.build(function (err, result) {
      msg.channel.sendCode('ruby', result);
    });
  } catch (error) {
    djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
  };
  }, {
    caseInsensitive: true,
    deleteCommand: true,
    description: 'Returns information on a mentioned user.',
    fullDescription: 'This command will give you information about a mentioned user on the given server that you use it on. Some information will change from server to server.',
    dmOnly: false,
    guildOnly: false,
    level: 5,
    usage: '@mention'
  });
};
