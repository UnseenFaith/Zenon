const League = require('../Modules/League.js');
const request = require('request');

exports.command = function(djs, cH) {
cH.registerCommand("league", function (msg, args) {
  try {
    if (args[0] === undefined) {
      msg.reply("Please specify a league name to search for.");
      return;
    }
    var summonerName = args.join('%20').toLowerCase();
    var region = 'na';
    var summoner = summonerName.replace(/%20/g, '');
    League.getSummonerByName(summonerName, region).then(body => {
      if (body.status === undefined) {
          let summObject = body[summoner];
          msg.channel.sendMessage(`**Summoner Name:** ${summObject.name}\n**Summoner ID:** ${summObject.id}\n**Summoner Level:** ${summObject.summonerLevel}\n**Summoner Profile Icon:** http://ddragon.leagueoflegends.com/cdn/6.20.1/img/profileicon/${summObject.profileIconId}.png`);
        } else {
          msg.channel.sendMessage(`**Status Code:** ${body.status.status_code}\n**Message:** ${body.status.message}`);
        }
    });
  } catch (error) {
    djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
  }
}, {
  aliases: ['lol'],
  caseInsensitive: true,
  deleteCommand: true,
  description: 'Returns basic information about a summoner.',
  fullDescription: 'This command will return basic information on a server. You can use the commands below to find out even more information. Defaults to the North American server. If you want to search on a different server, simply append a new region code on the end of the command.\n__Region Codes:__ **br, eune, euw, na, jp, kr, lan, las, oce, ru, tr**',
  usage: 'l3ap of faith **or** wickd EUW',
  level: 0
})
}
