var StringBuilder = require('stringbuilder');

exports.command = function(djs, cH) {
    cH.registerCommand("serverinfo", function(msg, args) {
        try {
            var sb = new StringBuilder({
                newline: '\r\n'
            });
            sb.appendLine('**Guild Information:  **');
            sb.appendLine("```ruby");
            sb.appendLine("Name:          " + msg.guild.name);
            sb.appendLine("ID:            " + msg.guild.id);
            sb.appendLine("Owner:         " + msg.guild.owner.user.username);
            sb.appendLine("Channels:      " + msg.guild.channels.array().length);
            sb.appendLine("Roles:         " + msg.guild.roles.array().length);
            sb.appendLine("Members:       " + msg.guild.memberCount);
            sb.appendLine("Region:        " + msg.guild.region.toUpperCase());
            sb.appendLine("Verification:  " + msg.guild.verificationLevel);
            sb.appendLine("AFK Channel:   " + (msg.guild.afkChannelID === null ? "No AFK Channel" : msg.guild.channels.get(msg.guild.afkChannelID).name));
            sb.appendLine("AFK Timeout:   " + (msg.guild.afkTimeout === null ? "No AFK Timeout" : ((msg.guild.afkTimeout) / 60 + " minutes")));
            sb.appendLine("Icon:          " + msg.guild.iconURL);
            sb.appendLine("```");
            sb.build(function(err, result) {
                msg.channel.sendMessage(result);
            });
        } catch (err) {
            console.log(err);
        }
    }, {
        aliases: ['guildinfo'],
        caseInsensitive: true,
        deleteCommand: true,
        description: 'Returns information on the current server/guild you\'re in',
        fullDescription: 'This command will return a ton of information related to the server/guild you use the command in.',
        level: 0
    });
}
