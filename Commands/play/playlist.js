const Music = require('../../Modules/Music.js');

exports.command = function(djs, cH) {
	cH.commands['play'].registerSubcommand("playlist", function(msg, args) {
		try {
			var author = msg.author;
			var id = args[0].toString().substring(args[0].indexOf('list='));
			var content = id.replace('list=', '');
			var number = parseInt(args[1]);
			if (Number.isNaN(number)) {
				number = 10;
			} else if (number > 50) {
				msg.reply("The max number of songs you can get is 50.");
				return;
			} else {
				number = number;
			}
			if (Music.checks(msg)) {
					Music.playList(content, msg, number)
							.then(response => {
									if (msg.guild.voiceConnection.music.stream.playing === false) {
											Music.play(msg);
									} else {
											return;
									};
							})
							.catch(error => {
									msg.channel.sendMessage(error);
							});
			};
		} catch (error) {
			djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
		}
	}, {
		caseInsensitive: true,
		deleteCommand: true,
		description: 'Plays music by using a playlist and an optional number parameter (max: 50) to add that many songs.',
		fullDescription: 'This command will query Youtube for playlist videos and then return the information, and then will play back the amount of videos you search for over the voice connection. The max amount of songs you can return is 50.',
		dmOnly: false,
		guildOnly: false,
		level: 0,
		usage: '<https://www.youtube.com/playlist?list=PLE4R0yRRPTKLh7T7Q5tn1TIAS2hpo1fZw> 50'
	});
};
