var YouTube = require('youtube-node');
var youtube = new YouTube();
const Music = require('../../Modules/Music.js');
const Constants = require('../../mysecretkeys.js').keys;


youtube.setKey(Constants.youtubekey);

exports.command = function(djs, cH) {
	cH.commands['play'].registerSubcommand("autoplay", function(msg, args) {
		try {
			var url = args.join(' ');
			if (!url.startsWith('https://www.youtube.com/watch?v=')) {
				msg.reply("You did not provide a valid link.")
				return;
			}
			var id = url.replace('https://www.youtube.com/watch?v=', '');
			if (id === '') {
				msg.reply("You did not provide a valid link.")
				return;
			}
			if (Music.checks(msg)) {
				youtube.getById(id, function(error, result) {
					if (error) return console.log(error);
					var jc = JSON.parse(JSON.stringify(result, null, 2));
					var arrayItems = jc.items;
					var title = arrayItems[0].snippet.title;
					let url = `https://www.youtube.com/watch?v=${id}`;
					msg.guild.voiceConnection.music.stream.queue.push({
						url: url,
						title: title
					});
					msg.channel.sendMessage(`\`[Music]\` Autoplay started with *${title}*`);
				});
						if (msg.guild.voiceConnection.music.stream.playing === false) {
										  msg.guild.voiceConnection.music.stream.autoplay = true;
											setTimeout(() => { Music.play(msg); }, 2000);
									} else {
											return;
									};
							}
		} catch (error) {
			djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
		}
	}, {
		aliases: ['auto', 'ap'],
		caseInsensitive: true,
		deleteCommand: true,
		description: 'Autoplays music from youtube using a starting URL.',
		fullDescription: 'This command will autoplay music from YouTube using a provided URL as the first video. Every video from then on will be from the "Up Next" feature of youtube.',
		level: 4,
		usage: '<https://www.youtube.com/watch?v=bM7SZ5SBzyY>'
	});
}
