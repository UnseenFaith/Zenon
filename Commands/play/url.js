const Music = require('../../Modules/Music.js');

exports.command = function(djs, cH) {
	cH.commands['play'].registerSubcommand("url", function(msg, args) {
		try {
			var content = args[0].toString();
			if (!content.startsWith("https://www.youtube.com/watch?v=")) {
				msg.reply("That is not a valid youtube link.");
				return;
			} else {
				var id = content.replace('https://www.youtube.com/watch?v=', "");
					if (Music.checks(msg)) {
							Music.getURL(id, msg)
									.then(response => {
											if (msg.guild.voiceConnection.music.stream.playing === false) {
													Music.play(msg);
											} else {
													return;
											};
									})
									.catch(error => {
											msg.channel.sendMessage(error);
									});
					};
				}
		} catch (error) {
			djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
		}
	}, {
		aliases: ['link'],
		caseInsensitive: true,
		deleteCommand: true,
		description: 'Plays music by using a Youtube URL you provide.',
		fullDescription: 'This command will query Youtube for information on the URL you provide and then queue up or play back the song over voice connection.',
		dmOnly: false,
		guildOnly: false,
		level: 0,
		usage: '<https://www.youtube.com/watch?v=zGERGT06AsM>'
	});
}
