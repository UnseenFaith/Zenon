exports.command = function(djs, cH) {
    cH.registerCommand("tempmute", function(msg, args) {
        try {
            if ((args[0] || args[1]) === undefined) {
                msg.reply("Invalid use of the command. Please refer to the usage of the command using the help command.");
                return;
            } else {
                let time = args[1];
                var mentions = msg.mentions.users.array();
                var id = mentions[0].id;
                if (id === undefined) {
                    msg.reply("You did not mention anyway to temporarily mute.");
                    return;
                } else if (time > 1800) {
                    msg.reply("You cannot mute someone for more than 30 minutes.");
                    return;
                } else {
                    msg.channel.overwritePermissions(id, {
                        SEND_MESSAGES: false
                    });
                    msg.channel.sendMessage(`**${mentions[0].username}** has been temporarily muted by **${msg.author.username}** for __${time}__ seconds.`);
                    setTimeout(() => {
                        msg.channel.overwritePermissions(id, {
                            SEND_MESSAGES: true
                        }), msg.channel.sendMessage(`**${mentions[0].username}** is now unmuted.`)
                    }, time * 1000);
                }
            }
        } catch (error) {
            djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
        }
    }, {
        aliases: ['tmute'],
        caseInsensitive: true,
        deleteCommand: true,
        description: 'Temporarily mutes a user',
        fullDescription: 'This command will temporarily mute a mentioned user for however many seconds you specify, as long as the number is under 30 minutes.',
        usage: '@mention 30',
        level: 2
    });
}
