const Music = require('../Modules/Music.js');

exports.command = function(djs, cH) {
	cH.registerCommand("resume", function(msg, args) {
		try {
			if (Music.checks(msg)) {
			var Stream = msg.guild.voiceConnection.music.stream;
			if (Stream.paused === true) {
        Stream.paused = false;
				Stream.dispatcher.resume();
        msg.channel.sendMessage(`\`[Music]\`Playback has been resumed by ${msg.author.username}`)
        return;
			} else if (Stream.paused === false) {
        msg.reply("There is already music playing.");
        return;
      }
		}
		} catch (error) {
			djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
		}
	}, {
		caseInsenitive: true,
		deleteCommand: true,
		description: 'Resumes the current playback.',
		fullDescription: 'This command will resume any playback on your server, whilst maintaining playback on others servers.',
		dmOnly: false,
		guildOnly: false,
		level: 1
	});
};
