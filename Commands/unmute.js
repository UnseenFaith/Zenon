exports.command = function(djs, cH) {
    cH.registerCommand("unmute", function(msg, args) {
        try {
            if (args[0] === undefined) {
                msg.reply("Invalid use of the command. Please refer to the usage of the command using the help command.");
                return;
            } else {
                var mentions = msg.mentions.users.array();
                var id = mentions[0].id;
                if (id === undefined) {
                    msg.reply("You did not mention anyway to temporarily mute.");
                    return;
                } else {
                    let permission = msg.channel.permissionOverwrites.find('id', id);
                    if (permission.denyData === 0) {
                      msg.reply("That user is not currently muted.");
                      return;
                    } else {
                    msg.channel.overwritePermissions(id, {
                        SEND_MESSAGES: true
                    });
                    msg.channel.sendMessage(`**${mentions[0].username}** has been unmuted by **${msg.author.username}**`);
                }
            }
          }
        } catch (error) {
            djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
        }
    }, {
        caseInsensitive: true,
        deleteCommand: true,
        description: 'Unmutes a permanently muted user.',
        fullDescription: 'This command will unmute a permanently muted user.',
        usage: '@mention',
        level: 2
    });
}
