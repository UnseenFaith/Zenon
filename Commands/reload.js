exports.command = function (djs, cH) {
    cH.registerCommand("reload", function (msg, args) {
        Object.keys(cH.commands).forEach(function (cmd, index) {
    //        if (cH.commands[cmd].label == 'help')
    //            return;
            cH.unregisterCommand(cH.commands[cmd].label);
        });
        var dive = require('dive');
            dive(process.cwd() + '/Commands', { recursive: true, directories: false, files: true, ignore: 'data' }, function (err, file, stat) {
                if (err) throw err;
                require(file).command(djs, cH);
                delete require.cache[require.resolve(file)];
            }, function () {
                msg.channel.sendMessage(`__[Reload]__ Commands have been reloaded!`);
            });
    }, {
            caseInsensitive: true,
            deleteCommand: true,
            description: 'Reloads all commands.',
            fullDescription: 'This command will reload all commands in the /Commands/ Folder, effectively creating a dynamic command framework.',
            level: 10
        });
}
