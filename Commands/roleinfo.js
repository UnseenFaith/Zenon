var StringBuilder = require('stringbuilder');

exports.command = function(djs, cH) {
    cH.registerCommand("roleinfo", function(msg, args) {
        try {
            var name = args.join(' ');
            if (name === undefined) {
              msg.reply("Improper use of the command.");
              return;
            }
            var role = msg.guild.roles.find('name', name);
            if (role === undefined) {
              msg.reply("That role does not exist on this guild.");
              return;
            }
            var members = role.members.array();
            var sb = new StringBuilder({
                newline: '\r\n'
            });
            sb.appendLine('**Role Information:  **');
            sb.appendLine("```ruby");
            sb.appendLine("Name:          " + role.name);
            sb.appendLine("ID:            " + role.id);
            sb.appendLine("Hoisted:       " + role.hoist);
            sb.appendLine("Color:         " + role.hexColor);
            sb.appendLine("Managed:       " + role.managed);
            sb.appendLine("Members:       " + role.members.size);
            sb.appendLine("Mentionable:   " + role.mentionable);
            sb.appendLine("Permissions:   " + role.permissions);
            sb.appendLine("Position:      " + role.position);
            sb.appendLine("Created at:    " + role.createdAt);
            sb.appendLine("```");
            sb.build(function(err, result) {
                msg.channel.sendMessage(result);
            });
        } catch (err) {
            console.log(err);
        }
    }, {
        aliases: ['rinfo'],
        caseInsensitive: true,
        deleteCommand: true,
        description: 'Returns information on the role of a server/guild you\'re in',
        fullDescription: 'This command will return a ton of information related to the role on the server/guild you use the command in.',
        level: 0
    });
}
