const google = require('google')

exports.command = function(djs, cH) {
    cH.registerCommand("google", function(msg, args) {
        try {
            if (args[0] === undefined) {
                msg.reply("Invalid use of the command. Please refer to the usage of the command using the help command.");
                return;
            } else {
                let query =  args.join(' ');
                msg.channel.sendMessage(`\`Google\` Searching for **${query}**`).then(msg2 => {
                google(query, function (err, res) {
                  if (err) {
                    console.log(err)
                  } else {
                    msg2.delete();
                    msg.channel.sendMessage(`**${res.links[0].title}**\n**${res.links[0].link}**`);
                  }
                })
            })
          }
        } catch (error) {
            djs.guilds.find('id', '195228461080510474').channels.find('name', 'bot-errors').sendMessage(error.stack);
        }
    }, {
        aliases: ['goog'],
        caseInsensitive: true,
        deleteCommand: true,
        description: '\'Googles\` for a specific link.',
        fullDescription: 'This command will \'Google\' for a specific search query and return a link.',
        usage: 'how to get soulstealer vayne',
        level: 0
    });
}
