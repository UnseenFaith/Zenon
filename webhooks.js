const Discord = require('discord.js');
const Constants = require('./mysecretkeys').keys;
const djs = new Discord.Client({ forceFetchUsers: true, autoReconnect: true });
const hook = new Discord.WebhookClient(Constants.webhookid, Constants.webhooktoken);
const http = require('http')
const createHandler = require('gitlab-webhook-handler2')
const handler = createHandler({ path: '/webhook', events: ['Push Hook', 'Issue Hook', 'Merge Request Hook', 'Note Hook']})

http.createServer(function (req, res) {
  handler(req, res, function (err) {
   res.statusCode = 404
   res.end('no such location')
  })
}).listen(7777)

handler.on('error', function (err) {
  console.error('Error:', err.message)
})

handler.on('Push Hook', function(event) {
    var pretext = "";
    event.payload.commits.forEach(function(commit, index) {
        if (index === event.payload.commits.length - 1) {
            pretext += `[\`${commit.id.substring(0, 8)}\`](${commit.url}) ${commit.message}`;
        } else {
            pretext += `[\`${commit.id.substring(0, 8)}\`](${commit.url}) ${commit.message}\n`;
        }
    });
    hook.sendSlackMessage({
        'attachments': [{
            'author_name': `${event.payload.user_name}`,
            'author_icon': `${event.payload.user_avatar}`,
            'text': `${event.payload.total_commits_count} new commits\n${pretext}`,
            'color': '#0000ff',
          //  'footer_icon': `${event.payload.project.avatar_url}`,
          //  'footer': `[${event.payload.project.name}](${event.payload.project.web_url})`
        }]
    });
});
handler.on('Issue Hook', function(event) {
    if (event.payload.object_attributes.action === 'open') {
        hook.sendSlackMessage({
            'attachments': [{
                'author_name': `${event.payload.user.name}`,
                'author_icon': `${event.payload.user.avatar_url}`,
                'title': `Issue opened: #${event.payload.object_attributes.iid} ${event.payload.object_attributes.title}`,
                'title_link': `${event.payload.object_attributes.url}`,
                'text': `${event.payload.object_attributes.description}`,
                'color': '#ff0000'
            }]
        });
    } else if (event.payload.object_attributes.action === 'close') {
        hook.sendSlackMessage({
            'username': 'Gitlab',
            'attachments': [{
                'author_name': `${event.payload.user.name}`,
                'author_icon': `${event.payload.user.avatar_url}`,
                'title': `Issue closed: #${event.payload.object_attributes.iid} ${event.payload.object_attributes.title}`,
                'title_link': `${event.payload.object_attributes.url}`,
                'color': '#008000'
            }]
        });
    } else {
      return;
    }
});

handler.on('Merge Request Hook', function (event) {
  console.log(event);
});

handler.on('Note Hook', function (event) {
  console.log(event);
})

djs.login(Constants.bottoken);

djs.on("ready", () => {
    console.log(`Webhooks ready!`);
});

//# sourceMappingURL=app.js.map
