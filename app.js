const CommandHandler = require("./Modules/CommandHandler.js");
const NodeCache = require( "node-cache" );
const myCache = new NodeCache();
const dive = require('dive');
const Discord = require('discord.js');
const Constants = require('./mysecretkeys.js').keys;
const djs = new Discord.Client({ forceFetchUsers: true, autoReconnect: true });
const cH = new CommandHandler({
  description: "a bot coded in JavaScript from the ground up using the discord.js framework.",
  ignoreBots: true,
  ignoreSelf: true,
  name: "Zenon",
  owner: "Faith#6948",
  prefix: "/",
});

global.volume;
global.volume = 20;
global.stream = {};

djs.login(Constants.bottoken);

djs.on("ready", () => {
    console.log(`Discord.js logged in.`);
});

djs.on("message", (msg) => {
  msg.command = false;
  if((!cH.commandOptions.ignoreSelf || msg.author.id !== djs.user.id) && (!cH.commandOptions.ignoreBots || !msg.author.bot) && (msg.prefix = cH.checkPrefix(msg))) {
      var args = msg.content.replace(/<@!/g, "<@").substring(msg.prefix.length).split(" ");
      var label = args.shift();
      label = cH.commandAliases[label] || label;
      var command;
      if((command = cH.commands[label]) !== undefined || ((command = cH.commands[label.toLowerCase()]) !== undefined && command.caseInsensitive)) {
          msg.command = true;
          var resp = (command.process(args, msg) || "").toString();
          if(resp) {
              msg.channel.sendMessage(resp);
          }
          if(command.deleteCommand) {
              msg.delete();
          }
      }
  }
});

dive(process.cwd() + '/Commands', { recursive: true, directories: false, files: true, ignore: 'data' }, function (err, file, stat) {
    if (err) throw err;
    require(file).command(djs, cH);
    delete require.cache[require.resolve(file)];
}, function () {
    console.log('Commands Loaded!');
});

process.on('uncaughtException', (error) => {
    if (error.code == 'ECONNRESET') {
        return;
    } else {
        console.log(error);
        process.exit(0);
    }
});

exports.djs = djs;
exports.myCache = myCache;
//# sourceMappingURL=app.js.map
